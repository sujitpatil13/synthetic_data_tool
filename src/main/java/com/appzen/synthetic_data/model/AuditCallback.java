package com.appzen.synthetic_data.model;

import com.az.expensereportscloud.bean.audit.AuditedExpenses;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class AuditCallback {

    @JsonProperty("cust_id")
    private Long customerId;

    @JsonProperty("report_id")
    private String reportId;

    @JsonProperty("expense_report")
    private AuditedExpenses expenseReport;
}
