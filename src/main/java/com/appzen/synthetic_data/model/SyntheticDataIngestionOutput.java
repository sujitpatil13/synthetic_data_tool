package com.appzen.synthetic_data.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class SyntheticDataIngestionOutput {

    @JsonProperty("report_number")
    private String reportNumber;
    @JsonProperty("risk_level")
    private String riskLevel;
    @JsonProperty("action")
    private String action;

    public SyntheticDataIngestionOutput() {
    }

    public SyntheticDataIngestionOutput(String reportNumber, String riskLevel, String action) {
        this.reportNumber = reportNumber;
        this.riskLevel = riskLevel;
        this.action = action;
    }
}
