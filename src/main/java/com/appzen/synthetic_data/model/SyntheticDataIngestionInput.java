package com.appzen.synthetic_data.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class SyntheticDataIngestionInput {
    @JsonProperty("run_id")
    private int runId;

    @JsonProperty("ingestion_host_url")
    private String ingestionHostUrl;

    @JsonProperty("console_host_url")
    private String consoleHostUrl;

    @JsonProperty("username")
    private String username;

    @JsonProperty("password")
    private String password;

    @JsonProperty("customer_id")
    private Long customerId;

    @JsonProperty("with_receipt")
    private boolean withReceipt;

    @JsonProperty("org_policy_map")
    private Map<String, List<String>> orgPolicyMap;

    @JsonProperty("report_number_seed")
    private int reportNumberSeed;

    @JsonProperty("report_name_pattern")
    private String reportNamePattern;

    @JsonProperty("submission_dates")
    private List<String> submissionDates;

    @JsonProperty("expense_types")
    private List<String> expenseTypes;

    @JsonProperty("expense_purposes")
    private List<String> expensePurposes;

    @JsonProperty("report_count")
    private Map<String,Integer> reportCount;

    @JsonProperty("user_details")
    private List<UserDetail> userDetails;

    @JsonProperty("random_lines")
    private Boolean isRandomLines;

    @JsonProperty("max_no_of_lines")
    private int maxNoOfLines;

    @JsonProperty("max_amount_per_line")
    private double maxAmountPerLine;

    @JsonProperty("risk_level")
    private List<String> riskLevel;

    @JsonProperty("action")
    private List<String> action;

    @Data
    public static class UserDetail {
        @JsonProperty("userName")
        private String userName;

        @JsonProperty("emailAddress")
        private String emailAddress;
    }
}
