package com.appzen.synthetic_data.config;


import com.appzen.config.AppZenCoreConfig;
import com.appzen.expense_submitted_postgres_writer_service.dao.EmployeeCompanyDetailsDao;
import com.appzen.globalcurrency.CurrencyClient;
import com.appzen.globalcurrency.config.CurrencyClientConfig;
import com.appzen.mysql_data_service.dao.UserProfileDao;
import com.appzen.mysql_data_service.dao.impl.UserProfileDaoImpl;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Scope;

@Configuration
@ComponentScan(basePackages = {"com.appzen.employee.sync","com.appzen.concur_v4_service","com.appzen.concur_service","com.appzen.expense_submitted_postgres_writer_service","com.appzen.customer"})
@Import({AppZenCoreConfig.class})
public class SyntheticDataConfig {

    @Bean(name = "clientConfig")
    @Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
    public CurrencyClientConfig clientConfig() {

        return new CurrencyClientConfig();
    }

    @Bean(name = "currencyClient")
    @Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
    public CurrencyClient currencyClient() {

        return new CurrencyClient();
    }

    @Bean(name = "employeeCompanyDetailsDaoPostgresWriter")
    @Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
    public EmployeeCompanyDetailsDao employeeCompanyDetailsDao() {

        return new EmployeeCompanyDetailsDao();
    }

    @Bean
    public UserProfileDao userProfileDao() {
        return new UserProfileDaoImpl();
    }
}
