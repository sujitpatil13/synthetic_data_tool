package com.appzen.synthetic_data;

import com.appzen.audit.model.ExpenseReport;
import com.appzen.audit.model.ExpenseReportLine;
import com.appzen.backend.expense.ExpenseBO;
import com.appzen.expense_submitted_postgres_writer_service.processor.PgExpensesProcessor;
import com.appzen.synthetic_data.dao.ExpenseLinesDao;
import com.appzen.synthetic_data.model.AuditCallback;
import com.appzen.synthetic_data.model.SyntheticDataIngestionOutput;
import com.az.expensereportscloud.bean.audit.AuditedExpenseLines;
import com.az.expensereportscloud.bean.audit.AuditedExpenses;
import com.az.expensereportscloud.bean.audit.ReportLevelAuditDetails;
import com.az.expensereportscloud.bean.audit.ReportLevelAudits;
import com.az.expensereportscloud.bean.audit.RiskObject;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.appzen.synthetic_data.IngestSyntheticDataWith.readTemplateFile;

@Slf4j
public class AuditSyntheticDataWith {

    private ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    private PgExpensesProcessor PG_EXPENSES_PROCESSOR = new PgExpensesProcessor();
    private ExpenseLinesDao expenseLinesDao = new ExpenseLinesDao();

    private RestTemplate restTemplate;
    private static final String EXPENSE_REPORT_AUDIT_TEMPLATE = "expense_report_audit_template_%s.json";

    public AuditSyntheticDataWith(PgExpensesProcessor pgExpensesProcessor) {

        this.restTemplate = new RestTemplate();
        this.PG_EXPENSES_PROCESSOR = pgExpensesProcessor;

    }

    public void makeAuditCallbackRequest(List<String> loginResponseHeaders, SyntheticDataIngestionOutput syntheticDataIngestionOutput, AuditedExpenses auditedExpenses, String consoleHostUrl, Long customerId) throws Exception {
        String uriString = UriComponentsBuilder.fromHttpUrl(consoleHostUrl)
                .path("console/rest")
                .pathSegment("auditcallback")
                .build().toUriString();

        HttpHeaders headers = buildHeaders(loginResponseHeaders);
        AuditCallback auditCallback = new AuditCallback();
        auditCallback.setCustomerId(customerId);

        auditCallback.setReportId(syntheticDataIngestionOutput.getReportNumber());
        auditCallback.setExpenseReport(auditedExpenses);

        String auditPayload = OBJECT_MAPPER.writeValueAsString(auditCallback);
        log.info("auditPayload:{}", auditPayload);
        HttpEntity<String> request = new HttpEntity<>(auditPayload, headers);


        restTemplate.exchange(uriString, HttpMethod.POST, request, String.class);
    }

    public AuditedExpenses buildSyntheticAuditData(SyntheticDataIngestionOutput syntheticDataIngestionOutput, String inputFileDir, Long customerId) throws ExpenseBO.ReportNotFoundException {
        AuditedExpenses auditedExpenses = readTemplateFile(inputFileDir + File.separator + String.format(EXPENSE_REPORT_AUDIT_TEMPLATE, syntheticDataIngestionOutput.getRiskLevel().toLowerCase()), AuditedExpenses.class);
        List<AuditedExpenseLines> auditedLines = fetchAuditedLines(auditedExpenses.getElines(), syntheticDataIngestionOutput, customerId);

        auditedExpenses.setReportNumber(syntheticDataIngestionOutput.getReportNumber());


        // Set<String> riskSet = auditedLines.stream().map(line -> line.getErisk().getLevel()).collect(Collectors.toSet());
//        String action = riskSet.contains("HIGH") ? "manual" : "Approve";

        auditedExpenses.setAction(syntheticDataIngestionOutput.getAction());
        log.info("action:{}", auditedExpenses.getAction());
//        String riskScore = riskSet.contains("HIGH") ? "HIGH" : (riskSet.contains("MEDIUM") ? "MEDIUM" : "LOW");
        auditedExpenses.setRisk_score(syntheticDataIngestionOutput.getRiskLevel());
        auditedExpenses.setElines(auditedLines);
        setReportLevelAudits(auditedExpenses, syntheticDataIngestionOutput.getRiskLevel());
        return auditedExpenses;
    }

    private List<AuditedExpenseLines> fetchAuditedLines(List<AuditedExpenseLines> elines, SyntheticDataIngestionOutput syntheticDataIngestionOutput, Long customerId) throws ExpenseBO.ReportNotFoundException {
        ExpenseReport expenseReport = PG_EXPENSES_PROCESSOR.getExpenseReport(customerId, syntheticDataIngestionOutput.getReportNumber());
        if (Objects.isNull(expenseReport)) {
            log.error("{} ReportNotFound for customer:{} in db", syntheticDataIngestionOutput.getReportNumber(), customerId);
            throw new ExpenseBO.ReportNotFoundException("ReportNotFound for customer");
        }
        Long expenseReportId = expenseReport.getExpenseReportId();
        List<ExpenseReportLine> expenseReportLines = expenseLinesDao.getExpenseReportLines(customerId, expenseReportId);

        return expenseReportLines.stream()
                .map(reportLine -> {
                    double amount = reportLine.getTransactionAmount().doubleValue();
                    return getAuditedLines(elines.get(0), amount, reportLine.getLineId().toString(),
                            syntheticDataIngestionOutput.getRiskLevel()
                    );
                })
                .collect(Collectors.toList());
    }

    private static AuditedExpenseLines getAuditedLines(AuditedExpenseLines reportTemplateLine, double amount, String elineid, String riskLevel) {
        AuditedExpenseLines auditedExpenseLines = new AuditedExpenseLines();
        auditedExpenseLines.setElineid(elineid);

        auditedExpenseLines.setChosen_appzen_category("Phone");
        auditedExpenseLines.setMissingReceipt("Y");
        auditedExpenseLines.setOcr(reportTemplateLine.getOcr());


        auditedExpenseLines.setOcr_data(reportTemplateLine.getOcr_data());

        RiskObject erisk = reportTemplateLine.getErisk();
        erisk.setLevel(riskLevel);
        auditedExpenseLines.setErisk(erisk);
        return auditedExpenseLines;
    }


    public static void setReportLevelAudits(AuditedExpenses auditedExpenses, String riskScore) {
        ReportLevelAudits reportLevelAudits = new ReportLevelAudits();
        List<ReportLevelAuditDetails> reportLevelAuditDetailsList = new ArrayList<>();
        reportLevelAuditDetailsList.add(getReportLevelAuditDetails(riskScore, "This is mock audit data ", "Check Rule"));
        reportLevelAudits.setReport_level_audit_details(reportLevelAuditDetailsList);
        auditedExpenses.setReport_level_audits(reportLevelAudits);
    }

    private static ReportLevelAuditDetails getReportLevelAuditDetails(String riskLevel, String riskMessage, String rule) {
        ReportLevelAuditDetails reportLevelAuditDetails = new ReportLevelAuditDetails();
        reportLevelAuditDetails.setResult("Passed");
        reportLevelAuditDetails.setRisk_level(riskLevel);
        reportLevelAuditDetails.setRisk_message(riskMessage);
        reportLevelAuditDetails.setRule(rule);
        reportLevelAuditDetails.setValidation_data(Collections.emptyMap());
        return reportLevelAuditDetails;
    }

    public List<String> login(String username, String password, String consoleHostUrl) {

        log.info("Login with user : {}, consoleHost: {}", username, consoleHostUrl);
        String uriString = UriComponentsBuilder.fromHttpUrl(consoleHostUrl)
                .path("console")
                .pathSegment("j_spring_security_check")
                .build().toUriString();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        headers.set("x-az-app-id", "1006");
        String body = String.format("j_username=%s&j_password=%s", username, password);
        HttpEntity<String> request = new HttpEntity<>(body, headers);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity(uriString, request, String.class);
        HttpHeaders responseHeaders = responseEntity.getHeaders();
        final List<String> response = responseHeaders.get(HttpHeaders.SET_COOKIE);
        log.info("Login response : {}", response);
        return response;
    }



    private static HttpHeaders buildHeaders(List<String> responseHeaders) throws Exception {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("X-Requested-With", "XMLHttpRequest");
        headers.set("owasp_csrftoken", "");
        String csrf = extractToken("APPZEN_CSRFTOKEN", responseHeaders);
        headers.set("APPZEN_CSRFTOKEN", csrf);

        String cookie = responseHeaders.stream().map(str -> str.split(";")[0]).collect(Collectors.joining("; "));
        headers.set("COOKIE", cookie);
        return headers;
    }

    private static String extractToken(String prefix, List<String> responseHeaders) throws Exception {
        String[] appzen_csrftokens = responseHeaders.stream()
                .filter(s -> s.startsWith(prefix))
                .map(s -> s.split("="))
                .map(strings -> strings[1]
                        .split(";"))
                .findFirst()
                .orElse(null);
        if (appzen_csrftokens == null || appzen_csrftokens.length == 0) {
            throw new Exception("Could not extract token from login");
        }

        return appzen_csrftokens[0];

    }


}
