package com.appzen.synthetic_data;


import com.appzen.backend.expense.ExpenseBO;
import com.appzen.backend.ingestionflow.processor.AppzenIngestionProcessor;
import com.appzen.expense_submitted_postgres_writer_service.processor.PgExpensesProcessor;
import com.appzen.ingestion.expense.stubs.DummyReceiptGenerator;
import com.appzen.ingestion.expense.stubs.ReceiptUploader;
import com.appzen.synthetic_data.config.SyntheticDataConfig;
import com.appzen.synthetic_data.model.SyntheticDataIngestionInput;
import com.appzen.synthetic_data.model.SyntheticDataIngestionOutput;
import com.az.expensereportscloud.bean.*;
import com.az.expensereportscloud.bean.audit.AuditedExpenses;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
public class IngestSyntheticDataWith implements CommandLineRunner {

    private static final Random RANDOM = new Random();
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    public static final String EXPENSE_LINE_TEMPLATE_JSON = "appzen_payload.json";
    private static Path inputFilePath;
    private static String inputFileDir;
    private static String inputFileName;
    private static Long customerId;
    private static AppzenIngestionProcessor appzenIngestionProcessor = new AppzenIngestionProcessor();
    private static DummyReceiptGenerator dummyReceiptGenerator = new DummyReceiptGenerator();
    private static ReceiptUploader receiptUploader = new ReceiptUploader();
    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    private AuditSyntheticDataWith auditSyntheticDataWith;
    private static AnnotationConfigApplicationContext context;
    private static List<String> loginResponseHeaders;
    private static int userDetailsCounter = 0;

    public IngestSyntheticDataWith(AnnotationConfigApplicationContext context) {
        auditSyntheticDataWith = new AuditSyntheticDataWith(context.getBean(PgExpensesProcessor.class));
    }


    public static void main(String[] args) {
        context = new AnnotationConfigApplicationContext(SyntheticDataConfig.class);
        try {
            inputFileDir = args[0];
            inputFileName = args[1];
            String userName = args[2];
            String password = args[3];
            log.info("inputFileDir:{} , inputFileName:{}", inputFileDir, inputFileName);
            inputFilePath = Paths.get(inputFileDir + File.separator + inputFileName);
            IngestSyntheticDataWith ingestSyntheticDataWith = new IngestSyntheticDataWith(context);
            SyntheticDataIngestionInput syntheticDataIngestionInput = OBJECT_MAPPER.readValue(inputFilePath.toFile(), SyntheticDataIngestionInput.class);
            syntheticDataIngestionInput.setUsername(userName);
            syntheticDataIngestionInput.setPassword(password);
            customerId = syntheticDataIngestionInput.getCustomerId();
            appzenIngestionProcessor.initialize(context);
            int reportNumberSeed = syntheticDataIngestionInput.getReportNumberSeed();
            log.info("reportNumberSeed:{}", reportNumberSeed);

            loginResponseHeaders = ingestSyntheticDataWith.getLoginToken(syntheticDataIngestionInput);

            for (Map.Entry<String, Integer> entry : syntheticDataIngestionInput.getReportCount().entrySet()) {
                for (int i = 0; i < entry.getValue(); i++) {
                    String reportNumber = String.format(syntheticDataIngestionInput.getReportNamePattern() + "_%d_%d", reportNumberSeed++, syntheticDataIngestionInput.getRunId());
                    try {
                        log.info("Ingesting report:{} with riskLevel:{}", reportNumber, entry.getKey());
                        ingestSyntheticDataWith.ingestReport(reportNumber, syntheticDataIngestionInput);
                        log.info("Ingestion completed report:{} with riskLevel:{}", reportNumber, entry.getKey());
                        if (!"UNAUDITED".equalsIgnoreCase(entry.getKey())) {
                            log.info("Auditing report:{} with riskLevel:{}", reportNumber, entry.getKey());

                            ingestSyntheticDataWith.auditSyntheticReport(reportNumber, syntheticDataIngestionInput, entry.getKey());
                            log.info("Audit completed report:{} with riskLevel:{}", reportNumber, entry.getKey());
                        }
                    } catch (JsonProcessingException e) {
                        log.error("JsonProcessingException:", e);
                    } catch (ExpenseBO.ReportNotFoundException e) {
                        log.error("ReportNotFoundException:", e);
                    } catch (Exception e) {
                        log.error("Exception:", e);
                    }
                }
            }
        } catch (JsonParseException e) {
            log.error("JsonParseException:", e);
        } catch (JsonMappingException e) {
            log.error("JsonMappingException:", e);
        } catch (IOException e) {
            log.error("IOException:", e);
        } catch (Exception e) {
            log.error("Exception:", e);
        } finally {
            context.close();
            System.exit(0);
        }
    }

    private List<String> getLoginToken(SyntheticDataIngestionInput syntheticDataIngestionInput) {
        return auditSyntheticDataWith.login(syntheticDataIngestionInput.getUsername(), syntheticDataIngestionInput.getPassword(), syntheticDataIngestionInput.getConsoleHostUrl());
    }

    private void ingestReport(String reportNumber, SyntheticDataIngestionInput syntheticDataIngestionInput) throws JsonProcessingException {

        Expenses expenses = getExpenseIRO(reportNumber, syntheticDataIngestionInput);
        expenses.setCustomerId(customerId);
        appzenIngestionProcessor.processSyntheticReport(customerId, expenses, null);

    }

    public static Expenses getExpenseIRO(String reportNumber, SyntheticDataIngestionInput syntheticDataInput) {
        Expenses expenses = null;
        //expenses = readTemplateFIle(inputFileDir + File.separator + EXPENSE_REPORT_TEMPLATE_JSON, Expenses.class);
        expenses = readTemplateFile(inputFileDir + File.separator + "appzen_payload.json", Expenses.class);
        expenses.setCustomerId(syntheticDataInput.getCustomerId());
        List<SyntheticDataIngestionInput.UserDetail> userDetailsList = syntheticDataInput.getUserDetails();
        if (userDetailsCounter == userDetailsList.size()) {
            userDetailsCounter = 0;
        }

        SyntheticDataIngestionInput.UserDetail userDetails = userDetailsList.get(userDetailsCounter++);
        expenses.setEmailAddress(userDetails.getEmailAddress());
        expenses.setUserid(userDetails.getEmailAddress());
        expenses.setUserName(userDetails.getUserName());
        expenses.setDate(generateRandomDate(syntheticDataInput.getSubmissionDates()));
        final Map<String, List<String>> orgPolicyMap = syntheticDataInput.getOrgPolicyMap();
        List<String> orgList = new ArrayList<>(orgPolicyMap.keySet());
        expenses.setOrgId(getRandomValue(orgList));
        expenses.setTemplateId(getRandomValue(orgPolicyMap.get(expenses.getOrgId())));
        List<ExpenseLineAttendees> elattendees = expenses.getElines().get(0).getElattendees();
        List<ExpenseLines> elines = buildELines(reportNumber, syntheticDataInput, elattendees, expenses.getElines().get(0));
        expenses.setElines(elines);
        expenses.setAmount(calculateReportLevelSum(elines));
        expenses.setReportNumber(reportNumber);
        expenses.setReportName(reportNumber);
        expenses.setForce(true);

        return expenses;
    }

    public static Expenses getExpenseIROFromTemplate(String reportNumber, SyntheticDataIngestionInput syntheticDataInput) {
        Expenses expenses = null;
        //expenses = readTemplateFIle(inputFileDir + File.separator + EXPENSE_REPORT_TEMPLATE_JSON, Expenses.class);
        expenses = readTemplateFile(inputFileDir + File.separator + "appzen_payload.json", Expenses.class);
        expenses.setCustomerId(syntheticDataInput.getCustomerId());
        List<SyntheticDataIngestionInput.UserDetail> userDetailsList = syntheticDataInput.getUserDetails();
        if (userDetailsCounter == userDetailsList.size()) {
            userDetailsCounter = 0;
        }

        SyntheticDataIngestionInput.UserDetail userDetails = userDetailsList.get(userDetailsCounter++);
        expenses.setEmailAddress(userDetails.getEmailAddress());
        expenses.setUserid(userDetails.getEmailAddress());
        expenses.setUserName(userDetails.getUserName());
        expenses.setDate(generateRandomDate(syntheticDataInput.getSubmissionDates()));
        final Map<String, List<String>> orgPolicyMap = syntheticDataInput.getOrgPolicyMap();
        List<String> orgList = new ArrayList<>(orgPolicyMap.keySet());
        expenses.setOrgId(getRandomValue(orgList));
        expenses.setTemplateId(getRandomValue(orgPolicyMap.get(expenses.getOrgId())));
        List<ExpenseLineAttendees> elattendees = expenses.getElines().get(0).getElattendees();
        //List<ExpenseLines> elines = buildELines(reportNumber, syntheticDataInput, elattendees, expenses.getElines().get(0));
        //expenses.setElines(elines);
        expenses.setAmount("100");
        expenses.setReportNumber(reportNumber);
        expenses.setReportName(reportNumber);
        expenses.setForce(true);

        return expenses;
    }

    private static List<ExpenseLines> buildELines(String reportNumber, SyntheticDataIngestionInput syntheticDataInput, List<ExpenseLineAttendees> elattendees, ExpenseLines expenseLine) {
        int noOfLines = 0;
        if (syntheticDataInput.getIsRandomLines()) {
            noOfLines = 1 + RANDOM.nextInt(syntheticDataInput.getMaxNoOfLines());
        } else {
            noOfLines = syntheticDataInput.getMaxNoOfLines();
        }
        final int numberOfLines = noOfLines;
        return Stream.iterate(1, n -> n + 1)
                .limit(noOfLines)
                .map((i) -> {
                    String elType = getRandomValue(syntheticDataInput.getExpenseTypes());
                    Double amount = syntheticDataInput.getMaxAmountPerLine() / numberOfLines;
                    final ExpenseLines line = getLine(reportNumber, syntheticDataInput, i.toString(), elType, String.valueOf(amount), elattendees, expenseLine);
                    try {
                        if (syntheticDataInput.isWithReceipt()) {
                            line.setElimageid(Collections.singletonList(generateReceipt(line, customerId)));
                        }
                    } catch (Exception e) {
                        log.error("Error while generating synthetic receipt for line;{}", line.getExpenseLineId());
                    }

                    return line;
                }).collect(Collectors.toList());
    }

    private static String generateReceipt(ExpenseLines expenseLine, long customerId) throws Exception {

        File receiptFile = dummyReceiptGenerator.generateReceiptFile(expenseLine);
        log.info("Uploading receipt {} to s3", receiptFile.toPath().toString());
        String imageId = receiptUploader.uploadImageToS3(customerId, expenseLine.getElimageid().get(0), receiptFile);
        Files.deleteIfExists(receiptFile.toPath());
        return imageId;
    }

    private static ExpenseLines getLine(String reportNumber, SyntheticDataIngestionInput syntheticDataInput, String elineId, String elType, String elAmount, List<ExpenseLineAttendees> elattendees, ExpenseLines expenseLine) {

        ExpenseLines expenseLines = new ExpenseLines();
        try {
            //expenseLine = readTemplateFile(inputFileDir + File.separator + EXPENSE_LINE_TEMPLATE_JSON, ExpenseLines.class);


            expenseLines.setElineid(elineId);
            if (!elattendees.isEmpty()) {
                int lineId = Integer.parseInt(elineId);
                if (isEvenLineId(lineId)) {
                    expenseLine.setElParentLineNumber(String.valueOf(lineId - 1));
                    expenseLine.setElattendees(elattendees);
                }
            }
            expenseLines.setEldate(generateRandomDate(syntheticDataInput.getSubmissionDates()));
            expenseLines.setEltype(elType);
            expenseLines.setElamount(elAmount);
            expenseLines.setElpaidamount(elAmount);
            expenseLines.setElpurpose(getRandomValue(syntheticDataInput.getExpensePurposes()));
            List<ExpenseLineAllocation> elallocs = expenseLine.getElallocs();
            if(elallocs.size() > 0) {
                elallocs.get(0).setAmount(elAmount);
            }
            expenseLines.setElallocs(elallocs);
            expenseLines.setElcurr(expenseLine.getElcurr());
            expenseLines.setElpaidcurr(expenseLine.getElpaidcurr());


            ///expenseLine.setel(buildElUniqueId(reportNumber, elineId, syntheticDataInput));
            final CardTransactionDetails cardTransactionDetails = new CardTransactionDetails();
            cardTransactionDetails.setTransactionMerchantName("Dummy Merchant");
            cardTransactionDetails.setMerchantCity("");
            cardTransactionDetails.setMerchantCountryCode("");
            cardTransactionDetails.setMerchantState("");
            expenseLines.setCardTransactionDetails(cardTransactionDetails);
        } catch (Exception e) {
            log.error("Exception in  getLine: {}", e);
        }


        return expenseLines;
    }

    private static boolean isEvenLineId(int i) {
        return i % 2 == 0;
    }

    private void auditSyntheticReport(String reportNumber, SyntheticDataIngestionInput syntheticDataIngestionInput, String riskLevel) throws Exception {
        SyntheticDataIngestionOutput syntheticDataIngestionOutput = new SyntheticDataIngestionOutput();
        syntheticDataIngestionOutput.setReportNumber(reportNumber);
        syntheticDataIngestionOutput.setRiskLevel(riskLevel);
        String action = "HIGH".equalsIgnoreCase(riskLevel) ? "manual" : "Approve";
        syntheticDataIngestionOutput.setAction(action);
        AuditedExpenses auditedExpenses = auditSyntheticDataWith.buildSyntheticAuditData(syntheticDataIngestionOutput, inputFileDir, customerId);
        auditSyntheticDataWith.makeAuditCallbackRequest(loginResponseHeaders, syntheticDataIngestionOutput, auditedExpenses, syntheticDataIngestionInput.getConsoleHostUrl(), customerId);
    }

    public static <T> T readTemplateFile(String filePathString, Class<T> type) {
        try {
            return OBJECT_MAPPER.readValue(Paths.get(filePathString).toFile(), type);
        } catch (IOException e) {
            log.error("IOException while reading template File:", e);
        }
        return null;
    }


    private static String generateRandomDate(List<String> submissionDates) {
        if (submissionDates.size() == 0) throw new RuntimeException("submissionDates required");
        Date startDate = null;
        Date endDate = null;
        try {
            if (submissionDates.size() == 1) {

                startDate = simpleDateFormat.parse(submissionDates.get(0));
            } else {
                startDate = simpleDateFormat.parse(submissionDates.get(0));
                endDate = simpleDateFormat.parse(submissionDates.get(1));
            }
        } catch (ParseException e) {
            log.error("ParseException in generateRandomDate:{}", e);
        }
        String pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
        DateFormat df = new SimpleDateFormat(pattern);
        if (endDate == null) {
            return df.format(startDate);
        }
        Date date = new Date(ThreadLocalRandom.current()
                .nextLong(startDate.getTime(), endDate.getTime()));

        return df.format(date);
    }

    private static String buildReportDescription(String reportNumber, SyntheticDataIngestionInput syntheticDataInput) {
        return String.format("%s_%s_%s_", syntheticDataInput.getReportNamePattern(), syntheticDataInput.getRunId(), reportNumber);
    }

    private static String buildElUniqueId(String reportNumber, String elineId, SyntheticDataIngestionInput syntheticDataInput) {
        return String.format("%d%s%s", syntheticDataInput.getRunId(), elineId, reportNumber);
    }

    private static String getRandomValue(List<String> valueList) {
        return valueList.get(RANDOM.nextInt(valueList.size()));
    }

    private static String calculateReportLevelSum(List<ExpenseLines> elines) {
        Optional<String> sum = elines.stream().map(ExpenseLines::getElamount)
                .map(Double::parseDouble).reduce(Double::sum).map(String::valueOf);
        return sum.orElse("0");
    }

    @Override
    public void run(String... args) throws Exception {
    }
}
