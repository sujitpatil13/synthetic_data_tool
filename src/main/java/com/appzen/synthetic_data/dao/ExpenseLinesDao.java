package com.appzen.synthetic_data.dao;

import com.appzen.audit.model.ExpenseReportLine;
import com.appzen.db.audit.tables.ExpenseReportLines;
import com.appzen.db.connection.AuditPoolReadOnly;

import java.util.List;

public class ExpenseLinesDao {

    public List<ExpenseReportLine> getExpenseReportLines(Long customerId, Long expenseReportId) {
        final List<ExpenseReportLine> expenseReportLineList = AuditPoolReadOnly.getDSLContext(customerId).select(
                ExpenseReportLines.EXPENSE_REPORT_LINES.EXPENSE_LINE_ID,
                ExpenseReportLines.EXPENSE_REPORT_LINES.EXPENSE_REPORT_ID,
                ExpenseReportLines.EXPENSE_REPORT_LINES.LINE_ID,
                ExpenseReportLines.EXPENSE_REPORT_LINES.TRANSACTION_AMOUNT,
                ExpenseReportLines.EXPENSE_REPORT_LINES.CUSTOMER_ID)
                .from(ExpenseReportLines.EXPENSE_REPORT_LINES)
                .where(ExpenseReportLines.EXPENSE_REPORT_LINES.EXPENSE_REPORT_ID.eq(expenseReportId).and(
                        ExpenseReportLines.EXPENSE_REPORT_LINES.CUSTOMER_ID.eq(customerId))
                ).fetchInto(ExpenseReportLine.class);

        return expenseReportLineList;
    }
}
