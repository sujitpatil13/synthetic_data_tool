package com.appzen.ingestion.expense.stubs;

import com.appzen.backend.ingestionflow.S3StoreManager;
import com.az.common.AZProperties;
import lombok.extern.slf4j.Slf4j;

import java.io.File;

@Slf4j
public class ReceiptUploader {
    private static final String IMAGES_S3_BUCKET = AZProperties.getInstance().getAwsBucketName();
    private final S3StoreManager s3StoreManager;

    public ReceiptUploader() {
        this.s3StoreManager = new S3StoreManager();
        this.s3StoreManager.initialize();
    }

    public String uploadImageToS3(Long customerId, String fileName, File imageFile) {
        String s3Key = customerId + "/" + fileName;
        log.info("Uploading mock image to S3 at location: {} in S3 bucket: {}", s3Key, IMAGES_S3_BUCKET);
        try {
            s3StoreManager.putObject(IMAGES_S3_BUCKET, s3Key, imageFile);
            return String.format("s3://%s/%s", IMAGES_S3_BUCKET, s3Key);
        } catch (Exception e) {
            String message = String.format("Error while uploading receipt image to s3 for customerId: %s", customerId);
            throw new ReceiptUploaderException(message, e);
        }
    }

    private static final class ReceiptUploaderException extends RuntimeException {
        public ReceiptUploaderException(String message, Throwable throwable) {
            super(message);
            log.error("ReceiptUploaderException: {}", message, throwable);
        }
    }
}
