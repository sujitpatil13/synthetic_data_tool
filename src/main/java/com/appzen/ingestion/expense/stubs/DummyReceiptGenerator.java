package com.appzen.ingestion.expense.stubs;


import com.az.expensereportscloud.bean.CardTransactionDetails;
import com.az.expensereportscloud.bean.ExpenseLines;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class DummyReceiptGenerator {
    private static final String DASHED_LINE = "------------------------------------------------------";
    private static final List<String> FOOD_ITEMS = Arrays.asList(
            "Fish Burger", "Fish & Chips", "Cheese Steak", "Barbeque Steak", "Soda", "Desserts", "Egg Noodles",
            "Soup", "Noodles", "Hakka Noodles", "Ice Cream", "Ham Burger", "Cheese Burger", "Cheese Pizza",
            "Cheese Pasta", "Coke", "Chicken Tikka", "Hot&Sour Soup", "Tandoori Chicken",
            "Nuggets", "Brownie", "Bread", "Hot Dog", "Omelette", "Cheese Omelette", "Puff", "Cheese Pizza",
            "Coffee", "Tea", "Ice Tea", "Latte", "Black Coffee", "Lime Water", "Green Tea"
    );
    public static final Font HELVETICA_BOLD = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD, BaseColor.BLACK);
    public static final Font HELVETICA = new Font(Font.FontFamily.HELVETICA, 13, Font.NORMAL, BaseColor.BLACK);

    private static String getRandomFoodItem() {
        return FOOD_ITEMS.get(ThreadLocalRandom.current().nextInt(DummyReceiptGenerator.FOOD_ITEMS.size()));
    }

    public File generateReceiptFile(ExpenseLines line) throws Exception {
        File outputPDF = File.createTempFile("Receipt_", ".pdf");
        buildReceiptPDF(outputPDF, line);
        return outputPDF;
    }

    private void buildReceiptPDF(File outputPDF, ExpenseLines line) throws IOException, DocumentException {
        try (FileOutputStream outputStream = new FileOutputStream(outputPDF)) {
            Rectangle pagesize = new Rectangle(288, 720);
            Document document = new Document(pagesize);
            PdfWriter writer = PdfWriter.getInstance(document, outputStream);
            document.open();
            document.newPage();
            buildReceiptContent(line, document);
            document.close();
            writer.close();
        }
    }

    private void buildReceiptContent(ExpenseLines line, Document document) throws DocumentException {
        CardTransactionDetails details = line.getCardTransactionDetails();
        document.add(addTextAtCenter(details.getTransactionMerchantName(), HELVETICA));
        document.add(addTextAtCenter(buildAddress(details), HELVETICA));
        document.add(Chunk.NEWLINE);
        addReceiptHeader(document, line);
        document.add(new Paragraph(DASHED_LINE));
        document.add(addTextAtCenter("TAKE AWAY\n", HELVETICA_BOLD));
        addRandomItems(document, line);
        document.add(addTextAtCenter("Thank you! Please visit us again!", HELVETICA));
    }

    private String buildAddress(CardTransactionDetails details) {
        String address = String.format("%s %s %s", details.getMerchantCity(), details.getMerchantState(),
                details.getMerchantCountryCode());
        return address;
    }

    private void addReceiptHeader(Document document, ExpenseLines line) throws DocumentException {
        String formattedDate = buildFormattedDate(line);
        int tableNumber = getRandomTableNumber();
        String header = String.format("Date: %s      Table No: %d", formattedDate, tableNumber);
        document.add(new Paragraph(header));
    }

    private int getRandomTableNumber() {
        return ThreadLocalRandom.current().nextInt(1, 25);
    }

    private String buildFormattedDate(ExpenseLines line) {
        DateTimeFormatter dateTimeOffsetFormatter = DateTimeFormatter.ofPattern("uuuu-MM-dd'T'HH:mm:ss.SSSX", Locale.ROOT);
        OffsetDateTime elDate = OffsetDateTime.parse(line.getEldate(), dateTimeOffsetFormatter);
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        return elDate.format(dateTimeFormatter);
    }

    private void addRandomItems(Document document, ExpenseLines line) throws DocumentException {
        double elAmount = Double.parseDouble(line.getElamount());
        Currency currency = Currency.getInstance(line.getElcurr());
        document.add(getItem(currency, getRandomFoodItem(), roundToTwoDecimals(elAmount * 0.35)));
        document.add(getItem(currency, getRandomFoodItem(), roundToTwoDecimals(elAmount * 0.25)));
        document.add(getItem(currency, getRandomFoodItem(), roundToTwoDecimals(elAmount * 0.4)));
        document.add(Chunk.NEWLINE);
        document.add(new Paragraph(DASHED_LINE));
        document.add(buildTotal(currency, roundToTwoDecimals(elAmount)));
    }

    private double roundToTwoDecimals(double amount) {
        return Math.round(amount * 100.0) / 100.0;
    }

    private PdfPTable buildTotal(Currency currency, double value) {
        PdfPTable table = new PdfPTable(3);
        table.setWidthPercentage(100);
        table.addCell(getCell(StringUtils.EMPTY, PdfPCell.ALIGN_LEFT));
        table.addCell(getCell("Total:", PdfPCell.ALIGN_MIDDLE));
        String totalAmount = String.format("%s %.2f", currency.getSymbol(), value);
        table.addCell(getCell(totalAmount, PdfPCell.ALIGN_RIGHT));
        return table;
    }

    private PdfPTable getItem(Currency currency, String item, double itemCost) {
        PdfPTable table = new PdfPTable(2);
        table.setWidthPercentage(100);
        String itemName = "1  " + item;
        table.addCell(getCell(itemName, PdfPCell.ALIGN_LEFT));
        String cost = String.format("%s %.2f", currency.getSymbol(), itemCost);
        table.addCell(getCell(cost, PdfPCell.ALIGN_RIGHT));
        return table;
    }

    public PdfPCell getCell(String text, int alignment) {
        PdfPCell cell = new PdfPCell(new Phrase(text));
        cell.setPadding(6);
        cell.setHorizontalAlignment(alignment);
        cell.setBorder(PdfPCell.NO_BORDER);
        return cell;
    }

    private Paragraph addTextAtCenter(String details, Font font) {
        Paragraph paragraph = new Paragraph(details, font);
        paragraph.setAlignment(Element.ALIGN_CENTER);
        return paragraph;
    }

}

class LineBuilder {
    static {
        System.setProperty("AZ_HOME", "/Users/lakshmikant.deshpande/code/azhome");
    }
    private static final String INPUT_JSON = "/Users/lakshmikant.deshpande/input.json";
    private static final Date START_DATE = new GregorianCalendar(2021, Calendar.FEBRUARY, 1).getTime();
    private static final Date END_DATE = new GregorianCalendar(2021, Calendar.FEBRUARY, 27).getTime();

    private static final Random RANDOM = new Random();
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    private static final List<String> MERCHANT_NAMES =
            Arrays.asList("McDonalds", "Dominos", "Moon Cafe", "KFC", "Eataly", "Roberta's Pizza", "Giordano’s",
                    "Lombardi’s", "Pequod’s Pizza", " Bar Bocce", "Grimaldi’s Pizzeria", "Piece", "Pizzeria Uno",
                    "Rubirosa", "Golden Boy Pizza", "Basic", "Lucali", "Joe's Pizza", "Evel Pie", "Organ Stop Pizza",
                    "Joe's Pizza", "Modern Apizza", "Secret Pizza", "Mani Osteria", "Spacca Napoli", "Huc-A-Poos",
                    "il Canale", "Urban Crust", "Evel Pie"
            );
    private static final List<String> EXPENSE_LINE_PURPOSES =
            Arrays.asList(
                    "Business Development", "Annual Training Conference", "Misc Office Expenses",
                    "Spring 2020 Education Fund Tuition Reimbursement", "Zambia Field Visit ",
                    "Camtasia Software", "Milage for PPIM/NACE/OTC", "Office Supply & FEDEX",
                    "Purchase batteries for Gamrey UPS", "UCI ISEB GBCI Design Review",
                    "HINO Motors", "CMC Travel Expenses", "staff recognition", "project expenses"
            );


}
